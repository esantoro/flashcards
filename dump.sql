PGDMP                     	    s            cards    9.3.9    9.3.9     �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �           1262    32769    cards    DATABASE     u   CREATE DATABASE cards WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.utf8' LC_CTYPE = 'en_US.utf8';
    DROP DATABASE cards;
             cards    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            �           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    5            �            3079    11756    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            �           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    174            �            1259    32785    decks    TABLE     �   CREATE TABLE decks (
    id integer NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    title character varying(255) NOT NULL,
    description character varying(255)
);
    DROP TABLE public.decks;
       public         cards    false    5            �            1259    32783    decks_id_seq    SEQUENCE     n   CREATE SEQUENCE decks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.decks_id_seq;
       public       cards    false    5    173            �           0    0    decks_id_seq    SEQUENCE OWNED BY     /   ALTER SEQUENCE decks_id_seq OWNED BY decks.id;
            public       cards    false    172            �            1259    32772    flash_cards    TABLE     "  CREATE TABLE flash_cards (
    id integer NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    title character varying(255) NOT NULL,
    front text NOT NULL,
    back text NOT NULL,
    deck_id integer
);
    DROP TABLE public.flash_cards;
       public         cards    false    5            �            1259    32770    flash_cards_id_seq    SEQUENCE     t   CREATE SEQUENCE flash_cards_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.flash_cards_id_seq;
       public       cards    false    5    171            �           0    0    flash_cards_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE flash_cards_id_seq OWNED BY flash_cards.id;
            public       cards    false    170            ,           2604    32788    id    DEFAULT     V   ALTER TABLE ONLY decks ALTER COLUMN id SET DEFAULT nextval('decks_id_seq'::regclass);
 7   ALTER TABLE public.decks ALTER COLUMN id DROP DEFAULT;
       public       cards    false    173    172    173            +           2604    32775    id    DEFAULT     b   ALTER TABLE ONLY flash_cards ALTER COLUMN id SET DEFAULT nextval('flash_cards_id_seq'::regclass);
 =   ALTER TABLE public.flash_cards ALTER COLUMN id DROP DEFAULT;
       public       cards    false    170    171    171            �          0    32785    decks 
   TABLE DATA               T   COPY decks (id, created_at, updated_at, deleted_at, title, description) FROM stdin;
    public       cards    false    173            �           0    0    decks_id_seq    SEQUENCE SET     3   SELECT pg_catalog.setval('decks_id_seq', 2, true);
            public       cards    false    172            �          0    32772    flash_cards 
   TABLE DATA               c   COPY flash_cards (id, created_at, updated_at, deleted_at, title, front, back, deck_id) FROM stdin;
    public       cards    false    171            �           0    0    flash_cards_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('flash_cards_id_seq', 13, true);
            public       cards    false    170            2           2606    32793 
   decks_pkey 
   CONSTRAINT     G   ALTER TABLE ONLY decks
    ADD CONSTRAINT decks_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.decks DROP CONSTRAINT decks_pkey;
       public         cards    false    173    173            .           2606    32780    flash_cards_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY flash_cards
    ADD CONSTRAINT flash_cards_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.flash_cards DROP CONSTRAINT flash_cards_pkey;
       public         cards    false    171    171            3           1259    32794    idx_decks_deleted_at    INDEX     E   CREATE INDEX idx_decks_deleted_at ON decks USING btree (deleted_at);
 (   DROP INDEX public.idx_decks_deleted_at;
       public         cards    false    173            /           1259    32782    idx_flash_cards_deck_id    INDEX     K   CREATE INDEX idx_flash_cards_deck_id ON flash_cards USING btree (deck_id);
 +   DROP INDEX public.idx_flash_cards_deck_id;
       public         cards    false    171            0           1259    32781    idx_flash_cards_deleted_at    INDEX     Q   CREATE INDEX idx_flash_cards_deleted_at ON flash_cards USING btree (deleted_at);
 .   DROP INDEX public.idx_flash_cards_deleted_at;
       public         cards    false    171            �   �   x�����0E��+��V��Ғ�B&�.�5�R�Hi'����L�J��9Z��u�U�(����)M�nU�R���E��3��-���N;��#]8��c��'b�#��H|W��۲E]����f�-EOKV��n��99�s �H��?�d_J)�j3N�      �   �  x��X�n�H}���4	V^$Q"��v&�6�Xۙ�F[lۍP���4Y��}�����؞jR/r�k����Ru���i9'���9�;�'��q`ۖ��}�̶`�}:�}%�y�ɓ��Z,ur"�l���޾۸��Ro)�q�k7�J���G_��xY����a�i{w&ĉsگm��~з-w����J?x�;�F�q�wKW~�y����E��$VW��9�T���'7*I�B�B��� ���C�F:V�k-���D�*�WS=W�d�P�Q�xj*����?+�l�=6�ӫ�ݍ�yS��sM��z��-�L�z��Y:��_��
�|�,�����Y����+Ȭ hy��g���J���OX�'�����-VjEI�q��t/��ᕣ�d��I����G����ZL���%<qO�D���e{����i��8���}��i�R��	�^2x��A[�A�� ���!T#~��RV���VΙ�,(C&�0�٣$<���Jňцf}�@t,���(�W���r)�\+$W���Vg��y��O�WYB�Ζ<�dI��j�	T�T�.�������m�3���_��9�i��A�	t֘q^��ڭPw�+�`m7i�&!�hJl'��%�G@UW�w+�;�oۺ�l�cI:6]j�m4�
��з�m�� 0���q��rSo7%lE��q?j~���r�PŢ���\?h��L҅, B9�/���A� �Е�;���ʊ�*�F��j�M��|zLő�UD�^&�H2����(��Ctg?D�T��=�{�a���Y�㣞�u����a�T��[A((��3jn�H-�}�$�Qຖ=���@��[��h8�ʴW��2�yr%KRp^���X
%|A3�H9̒2��t[�'\Bbj�5��@��㢒?7Xvi�v�vY����t��5����ckf[Ŭ4OP?��No.�"<��!
��v=z��Y��g�z�ХkU�cߞ*�Y�hK&9�5%O��ơ�8̸�W*�P��4x,v��s��5�a8C���Π�}78���ǯ����늁�Gɂ������(�-~|6G���p����E*[�h�T��/a�w( >��۽C��^&q ە�s��$�L!�|�b��z�7;=����(����*ɩ��uAa ��z�N�8��^s�5�+B�5qBLQ�N8��7o���ۜ1��h�:��֡���x���=2�r��T:^^�
oF��|?�3�j[����j�F
����Y���y"*��]��᯲}�w�J���4Z��Q���a��c迃�{��ʊ-�:h<���Τ�o@y���3��ȳ���_���*���B~�`E��a�[T���,�_5�z}�����Ώ����؞�f��ĺw�d�-X��4fI�-�R�\��wRr����}�#O���3a̡�"U
=�O���ø�� (�w�Q�׶2C�ɚ,��׸PO�#(2�VVG�U�<����J�7�d��w�wζS��5�\�o�UY���$�Dm�xd�V�~`C{����:;����i5�T��L��Ա���8�,��@�ެ.�55���Ab�Z�qq�{_����Gm\�V�R�t�H��R�����^�Y��fp�����Jz�-�]yD����mQD�o�D½�o���� n�����U^����U퐇�V_�M�?\�.��[ó�y�~��b�	ᦇ�!�P6�͸k7A$���q��ۖ5��1�ݴ"�S�;�/�1K���<�ڣku����8k���rD�� �Cΐ�Lòk�Z]��i$��(�Ǚ���8��FrdYV%1����-��&3��ʞ%�s�i�Q2�/8���7�q��^��h�3M��T��?��S��Ƙ˽I�-M��t�!։�#�39��B7���gtqw�D'���y��Jq1�̳bNEWz].k�m�����Di�����l��$����I�	ưm�r����՛ȳ�H(�251g :��?�1)0#���hm*�S���c����̬����E���     