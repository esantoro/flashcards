# FlashCards &mdash; beautiful flashcards


This app lets users author beautiful (Latex-grade typography) on the web.

FlashCards deck can be created and rendered into PDF files to be printed on a
double-side-printing-enabled printer.


# Platform: Dokku

Since Latex on Heroku is a real mess and I need this real quick for myself, I
moved the project from the Heroku platform to my own server running Dokku, a
small PAAS (Platform As A Service).

I might bring this back to heroku by building a custom buildpack:

https://sendgrid.com/blog/create-first-heroku-buildpack/

Note to readers: I already tried using other people's buildpacks and they
failed. I also tried customizing and patching the heroku-apt-buildpack, but
this approach also failed due to execution and import paths mismatch (.deb
packages are unpacked and installed in `/app/.apt/`, executables will look for
stuff like libraries and other executables in `/usr/` instead of `/app/.apt/user`).

I think the best approach would be to create a custom buildpack which still
installs to /app/.apt or something like that, but build custom packages by
passing correct values to build scripts (for example: `./configure
--prefix=/app/.apt/usr/local`).

So in the future, I might move this application back to Heroku.


## Useful links from the web:

- TeX/LaTeX interaction modes: http://tex.stackexchange.com/a/91624
(in particular: command line options on how to stop execution on errors instead
 of asking for input)

- Create your first buildpack: https://sendgrid.com/blog/create-first-heroku-buildpack/
