package controllers

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func RootController(ctx *gin.Context) {
	ctx.Redirect(http.StatusSeeOther, "/deck/list")
}
