package deck

import (
	"github.com/gin-gonic/gin"
	"net/http"

	"gitlab.com/esantoro/flashcards/models"
	"log"
	"strconv"
)

func DeckLatexExportController(ctx *gin.Context) {
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		log.Println("Error getting id, waaat")
	}

	var deck models.Deck

	// load deck from DB
	models.DB.First(&deck, id)

  ctx.String(http.StatusOK, deck.ToLatex())
}
