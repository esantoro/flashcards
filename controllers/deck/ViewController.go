package deck

import (
	"github.com/gin-gonic/gin"
	"net/http"
	// "github.com/jinzhu/gorm"

	"gitlab.com/esantoro/flashcards/models"
	"log"
	"strconv"
)

func ViewController(ctx *gin.Context) {

	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		log.Println("Error getting id, waaat")
	}

	var deck models.Deck
	var cards []models.FlashCard

	// load deck from DB
	models.DB.First(&deck, id)

	current_user, _ := models.GetUserByUsername(ctx.MustGet("username").(string))
	if deck.UserID != current_user.ID {
		// TODO: Flash a message to the user
		ctx.Redirect(http.StatusSeeOther, "/deck/list")
	}

	// find all cards for deck
	models.DB.Model(&deck).Related(&cards)

	ctx.HTML(http.StatusOK, "deck_view.html",
		gin.H{"deck": deck, "cards": cards})
}
