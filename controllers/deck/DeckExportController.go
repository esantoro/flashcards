package deck

import (
	"net/http"

	"github.com/gin-gonic/gin"
	// "github.com/jinzhu/gorm"

	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"strconv"

	"gitlab.com/esantoro/flashcards/models"
	// "net/url"
)

func DeckExportController(ctx *gin.Context) {
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		log.Println("Error getting id, waaat")
	}

	var deck models.Deck ;
	var DEBUG bool = models.DEBUG ;

	// load deck from DB
	models.DB.First(&deck, id)
	current_user, _ := models.GetUserByUsername(ctx.MustGet("username").(string))
	if deck.UserID != current_user.ID && (!deck.Public) {
		// TODO: Flash a message to the user
		ctx.Redirect(http.StatusSeeOther, "/deck/list")
	}

	latex_compilation_directory, err := ioutil.TempDir(os.TempDir(), "latex_compile")
	if err != nil {
		log.Println("TempDir Error:")
		log.Println(err)
	}
	texFile, err := os.Create(latex_compilation_directory + "/output.tex")
	if err != nil {
		log.Println("Error creating file:")
		log.Println(err)
		ctx.String(http.StatusOK, deck.ToLatex())
	}

	// log.Println("tmpdir", latex_compilation_directory)
	texFile.WriteString(deck.ToLatex())
	if err != nil {
		log.Println("TempDir Error:")
		log.Println(err)
	}

	pdflatex_command := "pdflatex"
	cmd := exec.Command(pdflatex_command, "-halt-on-error", "-output-directory",
		latex_compilation_directory,
		latex_compilation_directory+"/output.tex")

	cmd.Run()

	exportError := cmd.Wait()
	if exportError != nil && DEBUG == true {
		log.Println("[PdfExport] " + exportError.Error())
	}
	if cmd.ProcessState.Success() {
		// Serve the generated PDF file

		texFile.Close()
		
		// ctx.Header("Content-Disposition", fmt.Sprintf("attachment; filename=%s.pdf", url.QueryEscape(deck.Title)))
		// ctx.Header("Content-Disposition", fmt.Sprintf("inline; filename=%s.pdf", url.QueryEscape(deck.Title))) // BETTER
		ctx.Header("Content-Disposition", fmt.Sprintf("inline; filename=\"%s.pdf\"", deck.Title))
		ctx.Header("Content-Type", "application/pdf")
		ctx.File(latex_compilation_directory + "/output.pdf")

	} else {
		// show debug info
		var compilation_log []byte
		compilation_log, _ = ioutil.ReadFile(latex_compilation_directory + "/output.log")
		
		
		ctx.HTML(http.StatusInternalServerError,
			"deck_export_failure_log.html",
			gin.H{"compilation_log":string(compilation_log), "deck":deck}) ;
		// ctx.File(latex_compilation_directory + "/output.log")
	}

	// remove the temporary compilation directory
	os.RemoveAll(latex_compilation_directory)
	log.Println("[PdfExport] successfully removed the temporary compilation directory " + latex_compilation_directory)

}
