package deck

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func CreateController(ctx *gin.Context) {
	ctx.HTML(http.StatusOK, "deck_create.html", gin.H{})
}
