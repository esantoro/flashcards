package deck

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/esantoro/flashcards/models"
)

/*
  ExploreControler: this controller allow users to view the currently available
  public decks on the system.
*/

type pair struct {
	Deck   models.Deck
	Author models.User
}

func ExploreController(ctx *gin.Context) {
	log.Print("lollerplex")

	var publicDecks []models.Deck
	models.DB.Find(&publicDecks, "public=true")

	var user = new(models.User)

	data := make([]pair, len(publicDecks))

	for idx, deck := range publicDecks {
		models.DB.First(user, "id = ?", deck.UserID)
		data[idx] = pair{deck, *user}
	}

	log.Println("Public decks found: ", len(publicDecks))

	ctx.HTML(http.StatusOK, "decks_explore.html", gin.H{"data": data})
}
