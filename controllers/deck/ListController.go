package deck

import (
	"github.com/gin-gonic/gin"
	"net/http"
	// "github.com/jinzhu/gorm"

	"gitlab.com/esantoro/flashcards/models"
)

func ListController(ctx *gin.Context) {
	var decks []models.Deck

	current_user, _ := models.GetUserByUsername(ctx.MustGet("username").(string))

	models.DB.Where("user_id = ?", current_user.ID).Find(&decks)

	ctx.HTML(http.StatusOK, "deck_list.html", gin.H{"decks": decks})
}
