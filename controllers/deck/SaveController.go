package deck

import (
	"github.com/gin-gonic/gin"
	"github.com/gorilla/schema"
	"net/http"

	"fmt"
	"gitlab.com/esantoro/flashcards/models"
	"log"
)

var (
	decoder *schema.Decoder = schema.NewDecoder()
	err     error
)

func SaveController(ctx *gin.Context) {
	err = ctx.Request.ParseForm()

	current_user, _ := models.GetUserByUsername(ctx.MustGet("username").(string))


	if err != nil {
		// Handle error
		log.Println("[deck/save] error saving deck. Error:")
		log.Println(err)
	}

	var deck models.Deck
	// r.PostForm is a map of our POST form values
	err = decoder.Decode(&deck, ctx.Request.PostForm)
	// log.Println("Public:" , ctx.PostForm("public"))

	// set the deck to belong to the current user.
	deck.UserID = current_user.ID

	if err != nil {
		// Handle error
		log.Println("[deck/save] error saving deck (2). Error:")
		log.Println(err)
	}

	models.DB.Create(&deck)
	log.Println(deck)

	ctx.Redirect(http.StatusSeeOther, fmt.Sprintf("/deck/view/%d", deck.ID))
}
