package controllers

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func ChecksController(ctx *gin.Context) {
	ctx.String(http.StatusOK, "CHECKS_OK")
}
