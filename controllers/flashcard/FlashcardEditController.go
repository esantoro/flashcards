package flashcard

import (
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"strconv"

	// "github.com/gorilla/schema" // there is a "decoder" defined package-level
  // somewhere... I am not sure this is good.

	"gitlab.com/esantoro/flashcards/models"
  "fmt"
)

func FlashcardEditController(ctx *gin.Context) {
	request_method := ctx.Request.Method

	if request_method == "GET" {
		/*
		 * On GET, render a form with pre-filled fields for editing.
		 */
		id, err := strconv.Atoi(ctx.Param("id"))
		if err != nil {
			log.Println("Error getting ID")
		}
		var card models.FlashCard

		models.DB.First(&card, id)

		var all_decks []models.Deck
		models.DB.Find(&all_decks)

		ctx.HTML(http.StatusOK, "flashcard_edit.html",
			gin.H{"card": card, "user_decks": all_decks})

	} else if request_method == "POST" {
		/*
		 * On POST, perform update
		 */

		ctx.Request.ParseForm()

		id, err := strconv.Atoi(ctx.Param("id"))
		if err != nil {
			log.Println("Error getting ID")
		}

		var new_fc models.FlashCard
		err = decoder.Decode(&new_fc, ctx.Request.PostForm)

		if err != nil {
			log.Println("Error occurred decoding flashcard:")
			log.Println(err)
		} 

		var old_fc models.FlashCard
		models.DB.First(&old_fc, id)

		models.DB.Model(&old_fc).Updates(new_fc)

		ctx.Redirect(http.StatusSeeOther, fmt.Sprintf("/flashcard/view/%d", id))

	} else {
		// waaat
		log.Println("FlashcardEditController reached via method ", request_method)
		log.Println("This was not expected. Dunno what to do, returning a 500 error.")
	}
}
