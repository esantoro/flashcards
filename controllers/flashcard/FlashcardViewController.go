package flashcard

import (
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"strconv"

	"gitlab.com/esantoro/flashcards/models"
)

func FlashcardViewController(ctx *gin.Context) {
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		log.Println("Error getting ID")
	} else {
		log.Println("Got /view request for id", id)
	}

	var card models.FlashCard

	models.DB.First(&card, id)

	ctx.HTML(http.StatusOK, "flashcard_view.html", gin.H{"card": card})
}
