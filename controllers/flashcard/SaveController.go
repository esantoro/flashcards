package flashcard

import (
	"github.com/gin-gonic/gin"
	"github.com/gorilla/schema"
	"net/http"

	"fmt"
	"gitlab.com/esantoro/flashcards/models"
	"log"
)

var (
	decoder *schema.Decoder = schema.NewDecoder()
	err     error
)

func SaveController(ctx *gin.Context) {

	err = ctx.Request.ParseForm()

	if err != nil {
		// Handle error
		log.Println("[flashcard/save] error saving card. Error:")
		log.Println(err)
	}

	var fc models.FlashCard
	// ctx.Request.PostForm is a map of our POST form values
	err = decoder.Decode(&fc, ctx.Request.PostForm)

	if err != nil {
		// Handle error
		log.Println("[flashcard/save] error saving card (2). Error:")
		log.Println(err)
	}

	models.DB.Create(&fc)
	// log.Println(fc)
	// log.Println(fc.DeckID)
	ctx.Redirect(http.StatusSeeOther, fmt.Sprintf("/flashcard/view/%d", fc.ID))
	// ctx.String(http.StatusOK, "okay, ghe pensi mi")
}
