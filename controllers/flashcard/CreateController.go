package flashcard

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/esantoro/flashcards/models"
	"log"
	"net/http"
	"strconv"
)

func CreateController(ctx *gin.Context) {

	var deck_id int
	deck := ctx.Request.FormValue("deck")
	// no deck provided -- we'll let the user select the deck from the view
	if deck == "" {
		deck_id = -1 // magic value: IDs in the DB are greater than zero
		log.Println("[flashcard/create] unable to gather deck id")
	} else {
		var err error
		deck_id, err = strconv.Atoi(deck)
		if err != nil {
			deck_id = -1
		}
	}

	/*
		TODO: in this version, we have no users, so we query all of the decks.
		In the future, we will fetch only decks for the current user.
	*/

	var all_decks []models.Deck
	models.DB.Find(&all_decks)

	// log.Println("Creating flashcard for deck number ", deck_id)

	ctx.HTML(http.StatusOK,
		"flashcard_create.html",
		gin.H{"title": "Create a new flashcard",
			"deck_id":    deck_id,
			"user_decks": all_decks})
}
