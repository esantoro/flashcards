package controllers

/*
  This is the scratch controller: its only purpose is to let me try stuff
  within the context of a controller.
*/

import (
	"github.com/gin-gonic/gin"
	"net/http"

	"gitlab.com/esantoro/flashcards/models"
	"log"
)

func ScratchController(ctx *gin.Context) {

	var deck models.Deck
	var cards []models.FlashCard

	models.DB.First(&deck, 1)
	log.Println("Deck: ", deck)

	models.DB.Model(&deck).Related(&cards)

	log.Println("Cards for deck 1:")
	for d, c := range cards {
		log.Println("d: ", d)
		log.Println("c: ", c)
	}

	ctx.String(http.StatusOK, "This is okay")
}
