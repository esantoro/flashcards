package users

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/esantoro/flashcards/models"
)

/*
  UserPublicActivityController: This controller is supposed to show the
  public activity by the user.

  Things to expose:

  1) List of public decks
  2) History of deck update:
*/
func UserPublicActivityController(ctx *gin.Context) {
	// current_user, _ := models.GetUserByUsername(ctx.MustGet("username").(string))

	target_username := ctx.Param("username")
	if target_username == "" {
		// I should really implement flashes
		ctx.Redirect(http.StatusSeeOther, "/deck/list")
		return
	}

	target_user, err := models.GetUserByUsername(target_username)
	if err != nil {
		ctx.Redirect(http.StatusSeeOther, "/deck/list")
		return
	}

	var public_decks []models.Deck
	models.DB.Where("user_id = ? and public=true", target_user.ID).Find(&public_decks)
	ctx.HTML(http.StatusOK, "user_public.html",
		gin.H{"user": target_user, "decks": public_decks})

	log.Println("Number of public decks found: ", len(public_decks))
}
