package auth ;

import (
	"github.com/gin-gonic/gin"
	"net/http"
	// "github.com/jinzhu/gorm"

	// "gitlab.com/esantoro/flashcards/models"
	"log"
	"bytes"
	"github.com/gorilla/schema"
	"gitlab.com/esantoro/flashcards/models"

	"golang.org/x/crypto/bcrypt"
)

func SignUpController(ctx *gin.Context) {
  request_method := ctx.Request.Method

  if request_method == "GET" {
    ctx.HTML(http.StatusOK, "signup.html", gin.H{})
  } else if request_method == "POST" {
    // perform sign-up

		ctx.Request.ParseForm()
		decoder := schema.NewDecoder()

		var user models.User
		err := decoder.Decode(&user, ctx.Request.PostForm)
		if err != nil {
			ctx.Redirect(http.StatusSeeOther, "/signup")
		}

		user.PlainPassword = user.Password
		password_bytes := bytes.NewBufferString(user.Password).Bytes()

		hash, err := bcrypt.GenerateFromPassword(password_bytes, 3)
		if err != nil {
			log.Println("Error hashing password:")
			log.Println(err)
		}
		user.Password = string(hash)


		/*
		log.Println("username: ", user.Username)
		log.Println("email: ", user.Email)
		log.Println("password: ", user.Password)
		log.Println("plain_password: ", user.PlainPassword)
		*/

		models.DB.Create(&user)
		
		if models.DB.NewRecord(user) == false {
			ctx.Redirect(http.StatusSeeOther, "/login")
		} else {
			ctx.String(http.StatusOK, "nope")
		}
  }
}
