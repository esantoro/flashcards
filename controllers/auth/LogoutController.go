package auth ;

import (
  "net/http"
  "github.com/gin-gonic/gin"
  "github.com/gin-gonic/contrib/sessions"
)

func LogoutController(ctx *gin.Context) {
  session := sessions.Default(ctx)

  session.Clear()
  session.Save()

  ctx.Redirect(http.StatusSeeOther, "/login")
}
