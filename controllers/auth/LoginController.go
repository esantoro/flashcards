package auth ;


import (
	"log"
	"bytes"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/contrib/sessions"
	"gitlab.com/esantoro/flashcards/models"

	"golang.org/x/crypto/bcrypt"
)

func LoginController(ctx *gin.Context) {
  request_method := ctx.Request.Method

  if request_method == "GET" {
    ctx.HTML(http.StatusOK, "login.html", gin.H{})
  } else if request_method == "POST" {
		ctx.Request.ParseForm()

		username := ctx.PostForm("username")
		password := ctx.PostForm("password")

		user, err := models.GetUserByUsername(username)
		if err != nil {
			ctx.Redirect(http.StatusSeeOther, "/login")
		}

		password_attempt_bytes := bytes.NewBufferString(password).Bytes()
		password_hash_bytes := bytes.NewBufferString(user.Password).Bytes()

		login_err := bcrypt.CompareHashAndPassword(password_hash_bytes, password_attempt_bytes)

		if login_err != nil {
			log.Println("Login failure")
			ctx.Redirect(http.StatusSeeOther, "/login")
		} else {
			log.Println("Successfully logged in with user ", user.Username)

			session := sessions.Default(ctx)
			session.Set("username", user.Username)
			session.Save()

			ctx.Redirect(http.StatusSeeOther, "/deck/list")
		}





	}
}
