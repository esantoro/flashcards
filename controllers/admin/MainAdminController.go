package admin

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"runtime"

	"gitlab.com/esantoro/flashcards/models"
)

func MainAdminController(ctx *gin.Context) {

	/*

		Let's fetch some information about our userbase:
		- total number of users
		- total number of decks
		- total number of flashcards
	*/

	var users_count int
	var decks_count int
	var flashcards_count int

	models.DB.Model(&models.User{}).Count(&users_count)
	models.DB.Model(&models.Deck{}).Count(&decks_count)
	models.DB.Model(&models.FlashCard{}).Count(&flashcards_count)

	var mem runtime.MemStats
	runtime.ReadMemStats(&mem)

	mem.Alloc = mem.Alloc / (2 << 10)
	mem.TotalAlloc = mem.TotalAlloc / (2 << 10)
	mem.HeapAlloc = mem.HeapAlloc / (2 << 10)
	mem.HeapSys = mem.HeapSys / (2 << 10)

	stats_data := gin.H{
		"memstats":         mem,
		"users_count":      users_count,
		"decks_count":      decks_count,
		"flashcards_count": flashcards_count,
	}

	ctx.HTML(http.StatusOK, "admin_main.html", stats_data)
}
