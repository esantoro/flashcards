package middlewares ;

import (
  "strings"
  "net/http"
  "github.com/gin-gonic/gin"
  "github.com/gin-gonic/contrib/sessions"
)

/*
  User auth middleware: if the request comes from a logged-in user we load the
  session from the session-store and set the user object in the context.
*/
func UserAuthMiddleware(ctx *gin.Context) {
  target_path := ctx.Request.URL.Path ;

  if strings.HasPrefix(target_path, "/login") {
    ctx.Next()
    return
  } else if strings.HasPrefix(target_path, "/signup")  {
    ctx.Next()
    return
  } else {

    /*
     * check for username in session. if absent, redirect to /login
    */

    session := sessions.Default(ctx)

    user := session.Get("username")
    
    if user != nil {
      ctx.Set("username", user.(string))
      ctx.Next()
    } else {
      ctx.Redirect(http.StatusSeeOther, "/login")
    }

  }




}
