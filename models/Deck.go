package models

import (
	"github.com/jinzhu/gorm"
)

type Deck struct {
	gorm.Model

	Title       string      `schema:"title" sql:"not null"`
	Description string      `schema:"description"`
	FlashCards  []FlashCard `schema:"cards"`
	UserID      uint        `sql:"index"`
	Public      bool        `schema:"public" sql:"default:false"`
}

const (
	deck_latex_template_start = `\documentclass[avery5371,grid]{flashcards}
\usepackage{amssymb,amsmath}
\usepackage[utf8x]{inputenc}
\cardfrontstyle[\large\slshape]{headings}
\cardbackstyle{empty}

\begin{document}

`

	deck_latex_template_end = `\end{document}`
)

func (self *Deck) ToLatex() string {

	var cards []FlashCard

	DB.Order("created_at asc").Model(self).Related(&cards)

	output := deck_latex_template_start

	for _, card := range cards {
		output += "\n\n"
		output += card.ToLatex()
		output += "\n\n"
	}

	output += deck_latex_template_end

	return output
}
