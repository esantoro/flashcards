package models

import (
	"errors"
	"fmt"
	"github.com/jinzhu/gorm"
)

type User struct {
	gorm.Model
	Username      string `schema:"username" sql:"unique"`
	Password      string `schema:"password"`
	PlainPassword string
	Email         string `schema:"email" sql:"unique"`

	Decks []Deck
}

func GetUserById(id int) *User {
	var user User
	DB.First(&user, id)

	return &user
}

func GetUserByUsername(username string) (*User, error) {
	var user User
	DB.Where("username = ?", username).First(&user)
	// log.Println("[GetUserByUsername] pulled user '", user.Username, "' out of db")

	if user.Username != "" {
		return &user, nil
	} else {
		return &User{}, errors.New(fmt.Sprintf("No user with username %q", username))
	}
}
