package models

import (
	"github.com/jinzhu/gorm"
	_ "github.com/lib/pq"
	"log"
	"os"
	// "database/sql"
)

var (
    DEBUG	 bool
    DATABASE_URL string
    DB_TYPE      string
    DB  	 gorm.DB
    err 	 error
)

func init() {
	log.Println("[models/init] Beginning models initialization")

	if os.Getenv("DEBUG") == "true" {
	   DEBUG = true ;
	}
	

	/* Read variables from environment variables: */
	DATABASE_URL = os.Getenv("DATABASE_URL")
	if len(DATABASE_URL) == 0 {
		log.Fatal("[models/init] $DATABASE_URL was not set, exiting.")
	}

	DB_TYPE = os.Getenv("DB_TYPE")
	if len(DB_TYPE) == 0 {
		log.Fatal("[models/init] $DB_TYPE was not set, exiting.")
	} else if DB_TYPE == "postgresql" {
		log.Println("[models/init] $DB_TYPE was set to 'postgresql', fixing to 'postgres'")
		DB_TYPE = "postgres"
	}

	/* Initialize the database connections here. */
	DB, err = gorm.Open(DB_TYPE, DATABASE_URL)
	if err != nil {
		log.Println("[models/init] Failed to instantiate database connection. Error:")
		log.Fatal(err)
	}

	log.Println("[models/init] Registering model 'FlashCard'")
	DB.AutoMigrate(&FlashCard{})
	log.Println("[models/init] Registering model 'Deck'")
	DB.AutoMigrate(&Deck{})
	log.Println("[models/init] Registering model 'User'")
	DB.AutoMigrate(&User{})
}
