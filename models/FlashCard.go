package models

import (
	"fmt"
	"github.com/jinzhu/gorm"
)

type FlashCard struct {
	gorm.Model

	Title  string `schema:"title" sql:"not null"`
	Front  string `schema:"front" sql:"type:TEXT;not null"`
	Back   string `schema:"back"  sql:"type:TEXT;not null"`
	DeckID uint   `schema:"deck_id" sql:"index"`
}

const flashcard_latex_template string = `\cardfrontfoot{%s}
\begin{flashcard}{%s}
%s
\end{flashcard}`

func (self *FlashCard) ToLatex() string {
	var deck Deck
	DB.First(&deck, self.DeckID)

	return fmt.Sprintf(flashcard_latex_template, deck.Title, self.Front, self.Back)
}

/*

TODO: model registration should be done in model source file, but the "bug" here
is that apparently this source file is executed before init.go.


func init() {
  log.Println("[models/init] Registering model 'FlashCard'")
  DB.AutoMigrate(&FlashCard{})
}
*/
