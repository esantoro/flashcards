package main

import (
	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"

	"gitlab.com/esantoro/flashcards/controllers"
	"gitlab.com/esantoro/flashcards/controllers/auth"
	"gitlab.com/esantoro/flashcards/controllers/deck"
	"gitlab.com/esantoro/flashcards/controllers/flashcard"
	"gitlab.com/esantoro/flashcards/controllers/middlewares"
	"gitlab.com/esantoro/flashcards/controllers/users"
	_ "gitlab.com/esantoro/flashcards/models"

	"gitlab.com/esantoro/flashcards/controllers/admin"

	"fmt"
	"log"
	"os"
)

var (
	HTTP_PORT string
)

func main() {
	log.Println("[main] Starting up...")

	HTTP_PORT = os.Getenv("PORT")
	if len(HTTP_PORT) == 0 {
		log.Println("[main] $PORT was not set, defaulting to 5000.")
		HTTP_PORT = "5000"
	}

	session_store := sessions.NewCookieStore([]byte("I should definitely change this"))
	session_middleware := sessions.Sessions("session", session_store)

	// Router instantiation
	ROUTER := gin.Default()

	// Views and static paths
	ROUTER.LoadHTMLGlob("views/*")
	ROUTER.Static("/static", "./static")

	ROUTER.Use(session_middleware)
	ROUTER.Use(middlewares.UserAuthMiddleware)

	admin_routes := ROUTER.Group("/admin")
	{
		admin_routes.GET("/", admin.MainAdminController)
	}

	// Routes
	ROUTER.GET("/", controllers.RootController)
	ROUTER.GET("/checks", controllers.ChecksController)

	ROUTER.GET("/signup", auth.SignUpController)
	ROUTER.POST("/signup", auth.SignUpController)
	ROUTER.GET("/login", auth.LoginController)
	ROUTER.POST("/login", auth.LoginController)
	ROUTER.GET("/logout", auth.LogoutController)

	ROUTER.GET("/flashcard/create", flashcard.CreateController)
	ROUTER.POST("/flashcard/save", flashcard.SaveController)
	ROUTER.GET("/flashcard/view/:id", flashcard.FlashcardViewController)
	ROUTER.GET("/flashcard/edit/:id", flashcard.FlashcardEditController)
	ROUTER.POST("/flashcard/edit/:id", flashcard.FlashcardEditController)

	ROUTER.GET("/deck", deck.ListController)
	ROUTER.GET("/deck/", deck.ListController)
	ROUTER.GET("/deck/explore", deck.ExploreController)
	ROUTER.GET("/deck/list", deck.ListController)
	ROUTER.GET("/deck/create", deck.CreateController)
	ROUTER.POST("/deck/save", deck.SaveController)
	ROUTER.GET("/deck/view/:id", deck.ViewController)
	ROUTER.GET("/deck/export/:id", deck.DeckExportController)
	ROUTER.GET("/deck/latex_export/:id", deck.DeckLatexExportController)

	ROUTER.GET("/users/view/:username", users.UserPublicActivityController)

	ROUTER.GET("/scratch", controllers.ScratchController)

	log.Println("[main] Spawning web server interface.")
	ROUTER.Run(fmt.Sprintf(":%s", HTTP_PORT))
}
