function update_front() {
    var source_code = document.getElementById("front-source-code").value ;
    var target = document.getElementById("front-render-target") ;

    target.innerHTML = source_code ;

    MathJax.Hub.Queue(["Typeset",MathJax.Hub,target]);
}

function update_back() {
    var source_code = document.getElementById("back-source-code").value ;
    var target = document.getElementById("back-render-target") ;

    // target.innerHTML = "$$ \\text{ " + source_code + " } $$" ;
    target.innerHTML = source_code ;

    MathJax.Hub.Queue(["Typeset",MathJax.Hub,target]);
}
